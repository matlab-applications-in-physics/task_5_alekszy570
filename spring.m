%MATLAB 2020b
%name:spring 
%author: alekszy570
%date: 13.12.2020
%version: 1

rod_radius = 0.0021           %m                
turns_number = 40                               
weight = 0.1                  %kg               
time_constant = 10             %s           
gravity = 9.81                %m/(s^2)                  
roll_radius = 0.0025:0.001:0.025      %m
G = 8e10;                     %Pa
max_force = weight*gravity            %N
forcing_frequency = 1

beta = 1/2/time_constant                %Hz
elasticity_coefficient = (G*rod_radius^4)/(4*turns_number*roll_radius^3);
omega = sqrt(elasticity_coefficient./weight)
omega_1 = sqrt(omega.^2+beta.^2);

for i=1:size(roll_radius,2);
   amplitude(i) = max_force./(weight*sqrt((omega.^2 - omega_1.^2)^2+4*beta.^2*omega_1.^2));
end;

subplot(2,1,1)
plot(amplitude, roll_radius)
grid on;
title('Amplitude resonance curves in function of roll radius');
xlabel('roll radius m');
ylabel('amplitude m');

%subplot(2,1,2)
%plot(amplitude, roll_radius)
%grid on;
%title('Amplitude in function of roll radius');
%xlabel('roll radius m');
%ylabel('amplitude m');

